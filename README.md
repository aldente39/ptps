# README #

Prompt Themes for windows PowerShell (PTPS) はzshのpromptをPowerShellで再現しよう、という趣旨のプラグインです。


## インストール方法
ディレクトリをそのままパスの通ったところに置いてください。PowerShellのプロファイルには

```
#!powershell

$Env:PTPS_DIR = "PTPSのディレクトリのパス"
Invoke-Expression "$Env:PTPS_DIR\Set-Prompt.ps1"
```
と追記してください。

##つかいかた

引数なしでSet-Promptと入力し実行すると使い方が表示されます。
また、例えばテーマにadam1を設定したい場合は
```
#!powershell

Set-Prompt adam1
```
と入力します。プロファイルにこの1文を追記しておくと、新たにPowerShellを起動するときも最初からadam1が設定されます。
テーマによってはカラーリングも設定することができます。
例えば

```
#!powershell

Set-Prompt adam1 red green
```
とすると、色が変わります。

尚、使用できる色は、PowerShellに予め用意されているものに限られます。

##テーマについて
テーマはthemesディレクトリに保存されているものだけが、コマンドによる選択が可能になります。

新たにテーマを作成する場合は、"テーマ名.ps1"をthemesディレクトリに作成し、その中でprompt関数を定義してしてください。
カラーリングの設定を行えるようにする場合は、関数内に変数$Prompt_Colorを定義し、デフォルトのカラーを格納してください。
カラーリングを変更したい箇所の文字列を出力するときには

```
#!powershell

Write-Host "hoge" -ForegroundColor $Prompt_Color[0]   ###文字の前景色を変える場合
```
としてください。