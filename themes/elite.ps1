﻿##### elite for Windows PowerShell #####

function global:prompt {
    $time = Get-Date -uformat "%l:%M %p"
    $date = Get-Date -uformat "%m/%d"

    $location = $(Get-Location).Path
    if ($location.Equals($HOME)) {
        $location = $location.Replace($HOME, "~")
    }
    else {
        $location = $location.Split("\")[-1]
    }

    ### default color
    $Prompt_Color = "Red", "Blue"

    Write-Host -nonewline ┌┌ -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline "(" -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline $env:USERNAME -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline "@" -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline $env:USERDOMAIN -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline ")" -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline - -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline "(" -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline $time -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline -:- -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline $date -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline ")" -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline ┌- -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline ̈ -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline -̈̈ -ForegroundColor $Prompt_Color[0]
    Write-Host ˙ -ForegroundColor $Prompt_Color[1]

    Write-Host -nonewline └┌ -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline "(" -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline $location -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline ")" -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline ┌̈ -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline ˙ -ForegroundColor $Prompt_Color[1]

    return " "
}
