##### pws for Windows PowerShell #####

function global:prompt {
    $time = Get-Date

    Write-Host -nonewline $time.ToShortTimeString() -ForegroundColor Yellow -BackgroundColor DarkGray

    return "% "
}
