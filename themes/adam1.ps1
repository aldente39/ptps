##### adam1 for Windows PowerShell #####

function global:prompt {
    $size = $host.UI.RawUI.WindowSize.Width
    $location = $(Get-Location).Path
    $location = $location.Replace($HOME, "~")
    $name = $(whoami)
    $name = $name.Replace("\", "@")

    ### default color
    $Prompt_Color = "Blue", "Cyan", "Green"

    Write-Host -nonewline $name -ForegroundColor White -BackgroundColor $Prompt_Color[0]
    if ($location.Length -lt [math]::Truncate($size / 2)) {
        Write-Host -nonewline "" $location -ForegroundColor $Prompt_Color[1]
    }
    else {
        Write-Host "" $location -ForegroundColor $Prompt_Color[2]
    }

    return " % "
}
