##### adam2 for Windows PowerShell #####

function global:prompt {
    $size = $host.UI.RawUI.WindowSize.Width
    $location = $(Get-Location).Path
    $name = $(whoami)
    $name = $name.Replace("\", "@")
    $trunk = "-" * ($size - $location.Length - $name.Length - 8)

    ### default color
    $Prompt_Color = "Cyan", "Green", "Cyan", "White"

    $host.UI.RawUI.ForegroundColor = $Prompt_Color[3]

    Write-Host -nonewline ".-" -ForegroundColor $Prompt_Color[0]
    Write-Host -nonewline "(" -ForegroundColor Gray
    Write-Host -nonewline $location -ForegroundColor $Prompt_Color[1]
    Write-Host -nonewline ")" -ForegroundColor Gray

    Write-Host $trunk -nonewline -ForegroundColor $Prompt_Color[0]

    Write-Host -nonewline "(" -ForegroundColor Gray
    Write-Host -nonewline $name -ForegroundColor $Prompt_Color[2]
    Write-Host -nonewline ")" -ForegroundColor Gray
    Write-Host "-" -ForegroundColor $Prompt_Color[0]

    Write-Host -nonewline "``--" -ForegroundColor $Prompt_Color[0]

    return "> "
}
