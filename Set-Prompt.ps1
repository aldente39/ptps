##### Definition of a function "Set-Prompt" #####

$global:PTPS_current_theme = "default"

function global:Set-Prompt () {

    $usage = @"
Usage: Set-Prompt <options>
Options:
    -c             Show currently selected theme and parameters
    -l             List currently available prompt themes
    <theme>        Switch to new theme immediately
"@

    $theme_list = (ls -n ($Env:PTPS_DIR + "\themes\")).Replace(".ps1", "")

    if (![bool]$args[0]) {
        Write-Host $usage
    }
    elseif ($args[0].Equals("-c")) {
        Write-Host $PTPS_current_theme
    }
    elseif ($args[0].Equals("-l")) {
        Write-Host $theme_list
    }
    elseif ($theme_list.Contains($args[0])) {
        $prompt_str = [string]::join("`n", (Get-Content ($Env:PTPS_DIR + "\themes\" + $args[0] + ".ps1")))
        $global:PTPS_current_theme = $args[0]

        for ($i = 0; $i -lt $args.Length - 1; $i++) {
            $prompt_str = $prompt_str.Replace("`$Prompt_Color[" + $i + "]", "`"" +$args[$i + 1] + "`"")
            $global:PTPS_current_theme += " " + $args[$i + 1]
        }

        Invoke-Expression $prompt_str
    }
    else {
        Write-Host $usage
    }
}
